import React , {Component} from 'react';
import {Media } from 'reactstrap';
import { Card,CardImg,CardImgOverlay,CardText,CardBody,CardTitle } from 'reactstrap';
import DishDetail from './DishdetailComponent1'

class Menu extends Component {
    constructor(props){
        super(props);
        this.state = {
            selectedDish : null 
        }
        this.state={dishesDe:DishDetail}
       // console.log('Menu Component constructor is invoked');
    }

    // componentDidMount()
    // {
    //     console.log('Menu Component DidMount is invoked');
    // }
    onDishSelect(dish){
        this.setState({selectedDish:dish});
    }
    renderDish(dish){
        if(dish!=null){
            return(

                <Card>
                    <CardImg width="40%" src={dish.image} alt={dish.name} />
                        <CardBody>
                            <CardTitle>{dish.name}</CardTitle>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                     
                </Card>
                    );        
}
        else{
            return (
                <div>hello </div>
                );
            
        }
    }
    render(){
        const menu = this.props.dishes.map((dish)=> {

        return (
            <div  key= {dish.id} className="col-12 col-md-5 m-1">
                {/* <Media tag="li">
                    <Media left middle>
                        <Media object src={dish.image} alt={dish.name} />
                    </Media>
               
                <Media body className="ml-3">
                    <Media heading>{dish.name}</Media>
                    <p>{dish.description}</p>
                </Media>
                </Media> */}
                <Card  onClick={()=> this.onDishSelect(dish)}>
                    <CardImg width="100%" src={dish.image} alt={dish.name}/>
                     <CardImgOverlay>
                         <CardTitle>{dish.name}</CardTitle>
                        {/* <CardText>{dish.description}</CardText> */}
                     </CardImgOverlay>
                </Card>
             </div>
             
        );
        });
        console.log('Menu component render method is invoked')
        return(
            <div className="container">
                <div className="row">
                    {/* <Media list>
                        {menu}
                    </Media> */}
                    {menu}
                </div>
                <div className="row">
                <div className="col">
                    {/* <div className="col-12 col-md-5 m-1"> */}
                        
                        {this.renderDish(this.state.selectedDish)}
                    {/* </div> */}
                </div>
                <div className="col">
                
                <CardBody dishes={this.state.dishesDe}></CardBody>
                </div>
                </div>
            </div>
        
        
    );
    
        }
    }
export default Menu;