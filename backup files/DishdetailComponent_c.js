import React , {Component} from 'react';
import {Media } from 'reactstrap';
import { Card,CardImg,CardImgOverlay,CardText,CardBody,CardTitle } from 'reactstrap';
class DishDetail extends Component {
    // constructor(props){
    //     super(props);
        
       
    // }
     componentDidMount(){
        console.log("menu component componentDidMount is invoked");
    }

    renderDish(dish){
        // const dish= this.props.
            return(
                <div className="col-12 col-md-5 m-1">
                <Card>
                    <CardImg top src={dish.image} alt={dish.name} />
                        <CardBody>
                           <cardText>{dish.name}</cardText>
                            <CardText>{dish.description}</CardText>
                        </CardBody>
                </Card>
                </div>
                    );        
}
}
       renderComments(comments)
       {
           if(comments!=null)
           return(
               <div className="col-12 col-md-5 m-1">
                   <h4>comments</h4>
                   <ul className="List-umstyled">
                       {comments.map((comment)=> {
                           return(
                               <li key={comment.id}>
                               <p>{comment.comment}</p>
                               <p> -- {comment.author}, {new Intl.DateTimeFormat('en-US', { year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                               </li>
                           );
                       })}
                   </ul>
               </div>
           );
           else
           return (
               <div></div>
           );
            }
            render()
            {
                if(this.props.dish!=null)
                return (
                    <div className="container">
                        <div className="row">
                            {this.renderDish(this.props.dish)}
                            {this.renderComments(this.props.dish.comment)}
                        </div>
                    </div>
                );
                else
                return(
                    <div></div>
                );
            }
       }

    
}
export default DishDetail;